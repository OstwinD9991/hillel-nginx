<?php


namespace app;


use app\Interfaces\AlgorithmInterface;

class Argon2i implements AlgorithmInterface
{

    private static $Identifier= "PASSWORD_ARGON2I";
    private static $Options= ['memory_cost' => 10, 'time_cost'];

    public function getIdentifier(): string
    {
        return self::$Identifier;
    }

    public function getOptions(): array
    {
        return self::$Options;
    }

}