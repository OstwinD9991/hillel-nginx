<?php


namespace app;


use app\Interfaces\AlgorithmInterface;

class Bcrypt implements AlgorithmInterface
{

    private static $Identifier= "PASSWORD_BCRYPT";
    private static $Options= ['cost' => 10];

    public function getIdentifier(): string
    {
        return self::$Identifier;
    }

    public function getOptions(): array
    {
        return self::$Options;
    }

}