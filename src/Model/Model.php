<?php


namespace app\Model;


abstract class Model
{

//    public function __construct($data)
//    {
//        foreach ($data as $field=>$value){
//            $this->$field=$value;
//        }
//    }

    public static function find($id)
    {
        $pdo = new \PDO('mysql:host=mysql;dbname=docker', 'root', 'password');
        $table = self::pluralize("2", strtolower(end(explode("\\", get_called_class()))));

        $sql = "SELECT * FROM " . $table . " WHERE id= :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":id", $id);
        $stmt->execute();

        $data = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($data === false) {
            throw new \Exception("Beda");
        }
        return $data;
    }

    public function inset()
    {
        $pdo = new \PDO('mysql:host=mysql;dbname=docker', 'root', 'password');
        $table = self::pluralize("2", strtolower(end(explode("\\", get_called_class()))));
        $sql = "INSERT INTO " . $table . " (" . implode(", ", array_keys(get_object_vars($this))) . ") " . " VALUES " . " (:" . implode(", :", array_keys(get_object_vars($this))) . ")";
        $stmt = $pdo->prepare($sql);
        foreach (get_object_vars($this) as $field => $value) {
            if ($field == 'id' && $value === null) {
                $stmt->bindValue(':' . $field, $value, \PDO::PARAM_NULL);
                continue;
            }
            $stmt->bindValue(':' . $field, $value);
        };
        $stmt->execute();

    }

    public function update()
    {
        $pdo = new \PDO('mysql:host=mysql;dbname=docker', 'root', 'password');
        $table = self::pluralize("2", strtolower(end(explode("\\", get_called_class()))));
        foreach (array_keys(get_object_vars($this)) as $var) {
            if ($var == "id") {
                continue;
            }
            $vars[] = $var . "=" . ":" . $var;
        }
        $sql = "UPDATE " . $table . " SET " . implode(",", $vars,) . " WHERE id= :id";
        var_dump($sql);
        $stmt = $pdo->prepare($sql);
        foreach (get_object_vars($this) as $field => $value) {
            $stmt->bindValue(':' . $field, $value);
        };
        $stmt->execute();
    }

    public function delete()
    {
        $pdo = new \PDO('mysql:host=mysql;dbname=docker', 'root', 'password');
        $table = self::pluralize("2", strtolower(end(explode("\\", get_called_class()))));

        $sql = "DELETE FROM " . $table . " WHERE id= :id";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(":id", $this->id);
        $stmt->execute();
    }


    private static function pluralize($quantity, $singular, $plural = null)
    {
        if ($quantity == 1 || !strlen($singular)) return $singular;
        if ($plural !== null) return $plural;

        $last_letter = strtolower($singular[strlen($singular) - 1]);
        switch ($last_letter) {
            case 'y':
                return substr($singular, 0, -1) . 'ies';
            case 's':
                return $singular . 'es';
            default:
                return $singular . 's';
        }
    }
}