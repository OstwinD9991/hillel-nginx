<?php


namespace app\Model;


class ModelSQL

{
    public static function __callStatic($name, $arguments)
    {
        $sql = 'SELECT * FROM <table> WHERE ';
        var_dump($name);
        if (strpos($name, 'And') !== false) :
            $names_array = explode('And', $name);
            foreach ($names_array as $prefix) {
                $prefixsBy[] = self::nameBy($prefix);
                $prefixBetween[] = self::nameBetween($prefix);
                $prefixIn[] = self::nameIn($prefix);
            }
            $sql .= implode('', $prefixBetween);
            $sql .= implode(' AND ', $prefixsBy);
            $sql .= implode(' ', $prefixIn);
            return $sql;
        endif;
            $sql .= self::nameBy($name);
            $sql .= self::nameBetween($name);
            return $sql;

    }

    public static function nameBy($prefix)
    {
        if (strpos($prefix, 'By') !== false) :
            $tmp = str_replace('find', '', $prefix);
            $tmps = strtolower(str_replace('By', '', $tmp));
            $tmpsql = $tmps . ' = :' . $tmps;
            return $tmpsql;
        endif;
    }

    public static function nameBetween($prefix)
    {
        if (strpos($prefix, 'Between') !== false) :
            $tmp = str_replace('find', '', $prefix);
            $tmp = str_replace('Between', '', $tmp);
            $tmps = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $tmp));
            $tmpsql = $tmps . ' BETWEEN ' . self::prefixBetween();
            return $tmpsql;
        endif;
    }

    public static function prefixBetween()
    {
        $key = array("startDate", "endDate");
        foreach ($key as $keys) {
            $keyses[] = strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $keys));
        }
        foreach ($keyses as $tmpkey) {
            $tmpkeyses [] = ':' . $tmpkey;
        }
        $keys = implode(' AND ', $tmpkeyses);
        return $keys;
    }

    public static function nameIn($prefix)
    {
        if (strpos($prefix, 'In') !== false) {
            $tmp = substr($prefix, strpos($prefix, 'I'));
            $tmps = strtolower(str_replace('In', '', $tmp));
            $tmpsql = $tmps . ' IN';
            return $tmpsql;
        }
        return false;
    }
}
