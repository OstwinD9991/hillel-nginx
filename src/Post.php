<?php


namespace app;


final class Post extends Model\Model
{
    protected $id;
    protected $email;
    protected $name;

    public function __construct($id=null, $email, $name)
    {
        $this->id = $id;
        $this->email = $email;
        $this->name=$name;
    }

    public static function find($id)
    {
        $data=parent::find($id);
        return new self($data['id'], $data['email'], $data['name']);
    }
}