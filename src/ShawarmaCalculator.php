<?php


namespace app;


use app\Interfaces\ShawarmaInterface;

class  ShawarmaCalculator
{
    private static $bay_price;
    private static $bay_ingredients;


    public static function add(ShawarmaInterface ...$shawarma): void
    {
        foreach ($shawarma as $shawarm) {
            self::$bay_price[] = $shawarm->getCost();
            self::$bay_ingredients[] = $shawarm->getIngredients();
        }
    }

    public static function ingredients(): array
    {
        $tmp = null;
        foreach (self::$bay_ingredients as $ingredient) {
            if ($tmp !== null) {
                $tmp .= ", " . implode(", ", $ingredient);
            } else {
                $tmp = implode(", ", $ingredient);
            }
        }
        $shawerma_ingredients = array_unique(explode(", ", $tmp));
        return $shawerma_ingredients;
    }

    public static function price(): string
    {
        $price = array_sum(self::$bay_price);
        return $price;


    }

}