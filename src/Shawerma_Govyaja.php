<?php


namespace app;




class Shawerma_Govyaja implements \app\Interfaces\ShawarmaInterface
{
    private static $title = "Шаурма Говяжья";
    private static $cost = "75";
    private static $ingredients = ["Огурцы маринованные", "Чесночный соус", "Тандырный лаваш", "Маринованный лук с барбарисом и зеленью", "Говяжий окорок", "Салат коул слоу", "Помидоры свежие", "Хумус", "Соус тахин"];

    public function getTitle(): string
    {
        return self::$title;
    }

    public function getCost(): float
    {
        return self::$cost;
    }

    public function getIngredients(): array
    {
        return self::$ingredients;

    }


}