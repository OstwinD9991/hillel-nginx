<?php


namespace app;


class Shawerma_Odesskaya implements \app\Interfaces\ShawarmaInterface
{
    private static $title = "Шаурма Одесская";
    private static $cost = "69";
    private static $ingredients = ["Огурцы маринованные", "Картофель жареный", "Чесночный соус", "Тандырный лаваш", "Маринованный лук с барбарисом и зеленью", "Мясо куриное", "Салат коул слоу", "Корейская морковь"];

    public function getTitle(): string
    {
        return self::$title;
    }

    public function getCost(): float
    {
        return self::$cost;
    }

    public function getIngredients(): array
    {
        return self::$ingredients;

    }


}