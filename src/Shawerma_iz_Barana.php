<?php


namespace app;


class Shawerma_iz_Barana implements \app\Interfaces\ShawarmaInterface
{
    private static $title = "Шаурма из Баранины";
    private static $cost = "85";
    private static $ingredients = ["Огурцы маринованные", "Острый соус", "Лаваш арабский", "Маринованный лук с барбарисом и зеленью", "Мясо баранины", "Кинза", "Помидоры свежие"];

    public function getTitle(): string
    {
        return self::$title;
    }

    public function getCost(): float
    {
        return self::$cost;
    }

    public function getIngredients(): array
    {
        return self::$ingredients;

    }


}