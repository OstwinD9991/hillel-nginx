<?php


namespace app;


final class User extends Model\Model
{
    protected $id;
    protected $name;
    protected $pol;


    public function __construct($id=null, $name, $pol)
    {
        $this->id = $id;
        $this->name = $name;
        $this->pol=$pol;
    }

    public static function find($id)
    {
        $data=parent::find($id);
        return new self($data['id'], $data['name'], $data['pol']);
    }

}