<?php

namespace app\klasi;
class Currency
{
    private $isoCode;

    public function __construct($iso)
    {
        $this->setISO($iso);
    }

    private function setISO($iso)
    {
        if (!preg_match('/^[A-Z]{3}$/', $iso)) {
            throw new InvalidArgumentException();
        }
        $this->isoCode = $iso;
    }

    public function getISO()
    {
        return $this->isoCode;
    }

    public function equals(Currency $isoCode)
    {
        return $this->isoCode === $isoCode->getISO();
    }
}