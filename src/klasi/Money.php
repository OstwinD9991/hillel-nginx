<?php

namespace app\klasi;

class Money
{
    private $amount;

    /** @var Currency */
    private $currency;

    public function __construct($amount, Currency $isoCode)
    {
        $this->setamount($amount);
        $this->setcurrency($isoCode);

    }

    private function setamount($amount):void
    {
        $this->amount = $amount;
    }

    public function getamount()
    {
        return $this->amount;
    }

    public function setcurrency(Currency $isoCode)
    {
        $this->currency = $isoCode;
    }

    public function getcurrency() :Currency
    {
        return $this->currency;
    }

    public function equalsamount(Money $money)
    {
       if (!$this->currency ->equals($money->getcurrency()) ||  $this->amount !== $money->getamount())
       {
           return false;
       };
    }

    public function add(Money $money)
    {
        if (!$this->currency->equals($money->getcurrency())) {
            throw new InvalidArgumentException("Eror format");
        }
        $add= $this->amount += $money->getamount();
        return $add;

    }
}