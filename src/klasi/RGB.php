<?php


namespace app\klasi;


class RGB
{
    private $red;
    private $green;
    private $blue;


    public function __construct($red, $blue, $green)
    {
        $this->setRed($red);
        $this->setBlue($blue);
        $this->setGreen($green);

    }

    public function getRed()
    {
        return $this->red;
    }

    private function setRed($red): void
    {
        if (!($red >= 0 and $red <= 255)) {
            throw new InvalidArgumentException();
        }
        $this->red = $red;
    }

    public function getGreen()
    {
        return $this->green;
    }

    private function setGreen($green): void
    {
        if (!($green >= 0 and $green <= 255)) {
            throw new InvalidArgumentException();
        }
        $this->green = $green;
    }

    public function getBlue()
    {
        return $this->blue;
    }

    private function setBlue($blue): void
    {
        if (!($blue >= 0 and $blue <= 255)) {
            throw new InvalidArgumentException();
        }
        $this->blue = $blue;
    }

    public function equals(RGB $RGB): bool
    {
        if ($this == $RGB) {

            return true;
        }
        return false;
    }

    static function random(): RGB
    {
        $red = rand(0, 255);
        $green = rand(0, 255);
        $blue = rand(0, 255);
        return new RGB($red, $blue, $green);
    }

    public function mix(RGB $RGB): RGB
    {
        $mixedRed = ($this->red + $RGB->getRed()) / 2;
        $mixedGreen = ($this->green + $RGB->getGreen()) / 2;
        $mixedBlue = ($this->blue + $RGB->getBlue()) / 2;
        return new RGB($mixedRed, $mixedBlue, $mixedGreen);
    }


}